﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentCameraTexture : MonoBehaviour {


	public bool grabImage = false;



	Texture2D CurrentTexture;

	public Texture2D GetTexture
	{
		get { return CurrentTexture; }
	}
	private void OnPostRender()
    {

        // if(grabImage)
        // {
            //Create a new texture with the width and height of the screen
            Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            //Read the pixels in the Rect starting at 0,0 and ending at the screen's width and height
            texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
            texture.Apply();

            CurrentTexture = texture;
            Debug.Log("Got a new Texture");

            grabImage = false;
        // }
    }
}

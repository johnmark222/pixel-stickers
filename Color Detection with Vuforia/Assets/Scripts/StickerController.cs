﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class StickerController : MonoBehaviour 
{
	public Image Resizer;
	public List<DetectScript> Parts;
	public bool IsBeingDetected;
	public Texture2D CurrentTexture;
	public RawImage CurrentImageIndicator;
	public Transform TopPoint, BottomPoint;
	public Camera cam;

	public bool Test;

	private void Update() 
	{
		if(IsBeingDetected && CurrentTexture == null)
        {
			FindObjectOfType<CurrentCameraTexture>().grabImage = true;		
		}
		if(IsBeingDetected)
		{
			CurrentTexture = FindObjectOfType<CurrentCameraTexture>().GetTexture;
			//GetStickerTexture(CurrentTexture);
			Resizer.enabled = true;
			
			Vector2 Top = cam.WorldToScreenPoint(TopPoint.position);
			Vector2 Bot = cam.WorldToScreenPoint(BottomPoint.position);
			Rect temp = new Rect(Bot.x,Bot.y,(Top.x - Bot.x), Top.y - Bot.y);

			//CurrentImageIndicator.texture = CurrentTexture;
			Resizer.rectTransform.position = new Vector2(temp.x,temp.y);
			Resizer.rectTransform.sizeDelta = new Vector2(temp.width, temp.height);
		}
		else
		{
			Resizer.enabled = false;
		}
		if(Test)
		{
			GetStickerTexture(CurrentTexture);
			Test = false;
		}

		if(Input.GetMouseButtonDown(0))
		{
			if(this.transform.GetChild(0).GetComponent<Image>().enabled == true)
			{
				this.transform.position = Input.mousePosition;
			}
		}

		if(Input.GetMouseButton(1))
		{
			if(CurrentImageIndicator.enabled == true)
			{
				CurrentImageIndicator.transform.position = Input.mousePosition;
			}			
		}
	}
	
	public void GetStickerTexture()
	{
			Vector2 Top = cam.WorldToScreenPoint(TopPoint.position);
			Vector2 Bot = cam.WorldToScreenPoint(BottomPoint.position);
			//Create a new texture with the width and height of the screen
			Texture2D texture = CurrentTexture;
		if(Top.x < 1920 && Top.y < 1080 && Bot.x < 1920 && Bot.y < 1080)
		{
			if(Top.x > 0 && Top.y > 0 && Bot.x > 0 && Bot.y > 0)
			{
				
				//Read the pixels in the Rect starting at 0,0 and ending at the screen's width and height
				// Debug.Log("Top Position: " + Top + ", Bottom Position: " + Bot);
				// texture.ReadPixels(new Rect(Top.x, Top.y, Bot.x, Bot.y), 0, 0, false);
				// texture.Apply();
				Rect temp = new Rect(Bot.x,Bot.y,Top.x - Bot.x, Top.y - Bot.y);
				Debug.Log(temp);
				// Starting Position
				int x = Mathf.FloorToInt(temp.x);
				int y = Mathf.FloorToInt(temp.y);
				// Ending Position/Texture Size
				int width = Mathf.FloorToInt(temp.width);
				int height = Mathf.FloorToInt(temp.height);

				// The Color Pixel from left to right from the variables
				Color[] pixels = texture.GetPixels(x, y, width, height);
				Texture2D destTex = new Texture2D(width, height);
				// Texture2D destTex = new Texture2D(width - x, height - y);		
				// Debug.Log("Texture Width " + (width - x) + " Heigth " + (height - y));
				 destTex.SetPixels(pixels);
				 destTex.Apply();
				// Debug.Log(pixels.Length);
				// texture = destTex;
				CurrentImageIndicator.texture = destTex;
				CurrentImageIndicator.enabled = true;
			}
		}

            CurrentTexture = texture;
	}
	public void GetStickerTexture(Texture2D NewTexture)
	{
			Vector2 Top = cam.WorldToScreenPoint(TopPoint.position);
			Vector2 Bot = cam.WorldToScreenPoint(BottomPoint.position);
			//Create a new texture with the width and height of the screen
			Texture2D texture = NewTexture;
		if(Top.x < 1920 && Top.y < 1080 && Bot.x < 1920 && Bot.y < 1080)
		{
			if(Top.x > 0 && Top.y > 0 && Bot.x > 0 && Bot.y > 0)
			{
				
				//Read the pixels in the Rect starting at 0,0 and ending at the screen's width and height
				// Debug.Log("Top Position: " + Top + ", Bottom Position: " + Bot);
				// texture.ReadPixels(new Rect(Top.x, Top.y, Bot.x, Bot.y), 0, 0, false);
				// texture.Apply();
				Rect temp = new Rect(Bot.x,Bot.y,Top.x - Bot.x, Top.y - Bot.y);
				Debug.Log(temp);
				// Starting Position
				int x = Mathf.FloorToInt(temp.x);
				int y = Mathf.FloorToInt(temp.y);
				// Ending Position/Texture Size
				int width = Mathf.FloorToInt(temp.width);
				int height = Mathf.FloorToInt(temp.height);

				// The Color Pixel from left to right from the variables
				Color[] pixels = texture.GetPixels(x, y, width, height);
				Texture2D destTex = new Texture2D(width, height);
				// Texture2D destTex = new Texture2D(width - x, height - y);		
				// Debug.Log("Texture Width " + (width - x) + " Heigth " + (height - y));
				 destTex.SetPixels(pixels);
				 destTex.Apply();
				// Debug.Log(pixels.Length);
				// texture = destTex;
				CurrentImageIndicator.texture = destTex;
				CurrentImageIndicator.enabled = true;
				
			}
		}

            CurrentTexture = texture;
	}

	public void ApplyColors()
	{
		foreach(DetectScript a in Parts)
		{
			a.DetectRangeColorInTarget(CurrentTexture);
			a.imageToChange.enabled = true;
		}
	}

}
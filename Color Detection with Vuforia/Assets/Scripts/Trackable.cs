﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trackable : DefaultTrackableEventHandler
{

	[SerializeField]
	StickerController stickerTarget;

	protected override void	 OnTrackingFound()
	{
		base.OnTrackingFound();
		stickerTarget.IsBeingDetected = true;
	}

	protected override void OnTrackingLost()
	{
		base.OnTrackingLost();
		stickerTarget.IsBeingDetected = false;

	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DetectScript : MonoBehaviour 
{
    [Tooltip("Indicator for the color of the specified part of the image")]
	public Image imageToChange;
    [Tooltip("The target object that will be converted to screen to world point for detectin a color")]
    public Transform target;
    [Tooltip("Sticker target")]
    public StickerController sticker;

    [SerializeField]
    private int minimumRange = -5;
    [SerializeField]
    private int maximumRange = 5;

    private bool grabImage = false;
    [SerializeField]
    private Camera cam;
    [SerializeField]
    DefaultTrackableEventHandler tracker;
    
    void Update()
    {
        if(sticker.IsBeingDetected && CurrentTexture == null)
        {
            Debug.Log("Started Trying to grab image");
            cam.GetComponent<CurrentCameraTexture>().grabImage = true;
        }
		Vector3 screenPos = cam.WorldToScreenPoint(target.position);
        //Debug.Log("target is " + screenPos.x + " , " + screenPos.y);
        
        if(cam.GetComponent<CurrentCameraTexture>().GetTexture != null)
            CurrentTexture = cam.GetComponent<CurrentCameraTexture>().GetTexture;

        if(Input.GetKeyDown(KeyCode.Space))
        {
            DetectRangeColorInTarget();
        }
    }

    public void DetectColor()
    {
        grabImage = true;
        Debug.Log(" Initialized Color Detection");
        SetColor(GetColorOnPoint(GetPoint(target)));
    }

    public void DetectRangeColorIn(Transform target)
    {
        grabImage = true;
        Debug.Log(" Initialized Color Detection");
        StartCoroutine(GetColor(target));
    }

    public void DetectRangeColorInTarget()
    {
        grabImage = true;
        Debug.Log(" Initialized Color Detection");
        //StartCoroutine(GetColor());
        SetColor(GetAverageColorOnPoint(GetPoint(target)));
        
    }

    public void DetectRangeColorInTarget(Texture2D texture)
    {
        grabImage = true;
        Debug.Log(" Initialized Color Detection");
        //StartCoroutine(GetColor());
        SetColor(GetAverageColorOnPoint(GetPoint(transform), texture));
        
    }
    
    // Get average color based on the surrounding pixel colors
    Color GetAverageColorOnPoint(Vector2 point)
    {
        Color NewColor = Color.white;
        List<Color> NewColors = new List<Color>();

        for(int x = minimumRange; x < (maximumRange + 1); x++)
        {
            for(int y = minimumRange; y < (maximumRange + 1); y++)
            {
                NewColors.Add(CurrentTexture.GetPixel((int)point.x + x ,(int)point.y + y));
                
            }

        }

        // Gets the average of pixels
        foreach(Color c in NewColors)
        {
            NewColor += c;   
            NewColor.a = 1;
            Debug.Log(NewColor);
        }

        NewColor /= NewColors.Count;
        NewColor.a = 1;
        
        Debug.Log("Got Average Color");
        return NewColor;
    }
 
    Color GetAverageColorOnPoint(Vector2 point, Texture2D texture)
    {
        Color NewColor = Color.white;
        List<Color> NewColors = new List<Color>();

        for(int x = minimumRange; x < (maximumRange + 1); x++)
        {
            for(int y = minimumRange; y < (maximumRange + 1); y++)
            {
                NewColors.Add(texture.GetPixel((int)point.x + x ,(int)point.y + y));
                
            }

        }

        // Gets the average of pixels
        foreach(Color c in NewColors)
        {
            NewColor += c;   
            NewColor.a = 1;
            Debug.Log(NewColor);
        }

        NewColor /= NewColors.Count;
        NewColor.a = 1;
        
        Debug.Log("Got Average Color");
        return NewColor;
    }
 

	// Get the point the where the color needs to be detected
	Vector2 GetPoint(Transform target)
    {
        return cam.WorldToScreenPoint(target.position);
    }
   
    IEnumerator GetColor()
    {

        if(CurrentTexture != null)
        {
            SetColor(GetAverageColorOnPoint(GetPoint(target)));
            Debug.Log("Started Color Detection");
        }

        yield return null;
    }

    IEnumerator GetColor(Transform target)
    {

        if(CurrentTexture != null)
            SetColor(GetAverageColorOnPoint(GetPoint(target)));


        yield return null;

        
    }

	// Get the texture of the screen
    Texture2D CurrentTexture;

    // Compare the point using the texture
    Color GetColorOnPoint(Vector2 point)
    {
        return CurrentTexture.GetPixel((int)point.x,(int)point.y);
    }

    
    // Set the color of the image to the compared point

	void SetColor(Color setColor)
	{
        Debug.Log("Changing Color to " + (Color32)setColor + " in" + this.gameObject.name);
		imageToChange.color = setColor;
	}

    private void OnDrawGizmos() 
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 1);
    }

}

